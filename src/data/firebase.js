// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/firestore";

// TODO: Replace the following with your app's Firebase project configuration
var config = {
  apiKey: "AIzaSyBR5vRQNX4pH1dwAUJD0T1JPkp1VRDjtY8",
  authDomain: "museum-f1007.firebaseapp.com",
  databaseURL: "https://museum-f1007.firebaseio.com",
  projectId: "museum-f1007",
  storageBucket: "museum-f1007.appspot.com",
  messagingSenderId: "877386938378"
};

// Initialize Firebase
firebase.initializeApp(config);
const db = firebase.firestore();
export { db };