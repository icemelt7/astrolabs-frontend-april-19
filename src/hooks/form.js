import { useState } from 'react';
const useForm = (defaultValues) => {
  const [values, setValues] = useState(defaultValues);
  const onChange = (e) => {
    const value = {
      ...values,
      [e.target.name]: e.target.value
    }
    setValues(value);
  }
  return [values, onChange];
}

export { useForm }