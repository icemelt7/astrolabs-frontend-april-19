/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from 'react';
import { db } from './data/firebase';
import './App.css';
import checked from './images/checked.png';
import unchecked from './images/unchecked.png';

const App = () => {
  const [todos, setTodos] = useState({todos: []})
  useEffect(() => {
    db.collection("todos").onSnapshot((querySnapshot) => {
      const _todos = [];
      querySnapshot.forEach((doc) => {
        _todos.push({id: doc.id, ...doc.data()})
      });
      setTodos({todos: _todos});
    });
  }, []);
  
  const markDone = (id) => {
    /* const _todos = todos.todos.map( todo => {
      if (todo.id === id) {
        return { ...todo, status: 'done'}
      }
      return todo
    })
    setTodos({todos: _todos}) */
    db.collection("todos").doc(id).update({
      status: 'done'
    })
  }

  const markUndone = (id) => {
    /* const _todos = todos.todos.map( todo => {
      if (todo.id === id) {
        return { ...todo, status: 'not done'}
      }
      return todo
    })
    setTodos({todos: _todos}) */
    db.collection("todos").doc(id).update({
      status: 'not done'
    })
  }

  return <div className='app'>
    <Body>
      <Date />
      <Day />
      <TodoListHolder>
        {todos.todos.map( todo => <Todo todo={todo} markDone={markDone} markUndone={markUndone} key={todo.id} />)}
      </TodoListHolder>
    </Body>
  </div>
}

const Body = ({ children }) => {
  return <div className='body'>
    { children }
  </div>
}

const Date = () => {
  return <div className='date'>
    <div className='number'>12</div>
    <div className='month'>JAN 2016</div>
  </div>
}

const Day = () => {
  return <div className='day'>tuesday</div>
}

const TodoListHolder = ({ children }) => {
  return <div className='todo-list-holder'>{ children }</div>
}

const Todo = ({ todo, markDone, markUndone }) => {
  // const [ status, setStatus ] = useState('not done');
  const _markDone = () => {
    // setStatus('done')
    markDone(todo.id)
  }

  const _markUndone = () => {
    // setStatus('not done')
    markUndone(todo.id)
  }
  const status = todo.status;
  if (status === 'not done') { 
    return <div className='todo-undone' onClick={_markDone}>
      <div className='todo-text'>{todo.text}</div>
      <img className='todo-uncheck' src={unchecked} />
    </div>
  } else {
    return <div className='todo-done' onClick={_markUndone}>
    <div className='todo-text'>{todo.text}</div>
      <img className='todo-check' src={checked} />
    </div>
  }
}

export default App;
