import React from 'react';
import { Route } from 'react-router-dom';
import { Nav } from './components/Nav';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import { Home } from './pages/Home';

const App = () => {
  return <div>
    <Nav />
    <Route path={'/login'} component={Login} />
    <Route path={'/register'} component={Register} />
    <Route path={'/'} exact={true} component={Home} />
  </div>
}


export default App;