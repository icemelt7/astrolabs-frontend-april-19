import React from 'react';
import { useForm } from '../hooks/form';

const Register = () => {
  const [values, onChange] = useForm({});
  const submit = (e) => {
    e.preventDefault();
    console.log(values);

    fetch('https://reqres.in/api/register',{
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values)
    }).then((response) => {
      response.json().then(jsonResponse => {
        console.log(jsonResponse)
      })
    })
  }
  
  return <div>
  <form onSubmit={submit}>
    <input type='email' name='email' value={values.email} onChange={onChange} />
    
    <input type='password' value={values.password} onChange={onChange} name='password' />
    <input type='submit' value='register' />
    </form>
</div>
}

export { Register }