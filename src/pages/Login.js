import React from 'react';
import { useForm } from '../hooks/form';
const Login = () => {
  const [values, onChange] = useForm({});
  const submit = async(e) => {
    e.preventDefault();
    console.log(values);

    const response = await fetch('https://reqres.in/api/login',{
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values)
    })
    
    const jsonResponse = await response.json();
    console.log(jsonResponse);
  }

  return <div>
    <form onSubmit={submit}>
      <input type='email' name='email' value={values.email} onChange={onChange} />
      
      <input type='password' value={values.password} onChange={onChange} name='password' />

      <input type='submit' value='Submit' />
      </form>
  </div>


  // Step 1
  /* const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const submit = () => {

  }
  const onChangeEmail = (e) => {
    setEmail(e.target.value)
  }

  const onPassword = (e) => {
    setPassword(e.target.value)
  }

  return <div>
    <form onSubmit={submit}>
      <input type='email' name='email' value={email} onChange={onChangeEmail} />
      <input type='password' value={password} onChange={onPassword} name='password' />
    </form>
  </div> */
}

export { Login }